#include "player.h"
#include <iostream>
#include <map>
#include <list>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	board = new Board();
	playerside = side;
	if (playerside == BLACK)
	{
		opponentside = WHITE;
	}
	else
	{
		opponentside = BLACK;
	}
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}
//Testing
/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    board->doMove(opponentsMove, opponentside);
    int scoreBoard[8][8] = {0};
	 for (int i = 0; i < 8; i ++)//x coordinate
	 {
		 for (int j = 0; j < 8; j++)//y coordinate
		 {
			 Move *move= new Move(i, j);
			 if (board->checkMove(move, playerside))
			 {
				 if ((i == 0 && j == 1) || (i == 0 && j == 7) || (i == 7 && j == 7) || (i == 7 && j == 0))
				 {
					 scoreBoard[i][j] += 100;
				 }
				 else if (i == 0 || j == 0 || i == 7 || j == 7)
				 {
					 scoreBoard[i][j] += 50;
				 }
				 else if (i == 1 || i == 6 || j == 1 || j == 6)
				 {
					 scoreBoard[i][j] += 10;
				 }
				 else
				 {
					 scoreBoard[i][j] += 40;
				 }
			 }
		 }
	 }
	 int prefScore = 2;
	 int prefx = -1;
	 int prefy = -1;
	 for (int i = 0; i < 8; i++)
	 {
		 for (int j = 0; j < 8; j++)
		 {
			 if (scoreBoard[i][j] >= prefScore)
			 {
				 prefScore = scoreBoard[i][j];
				 prefx = i;
				 prefy = j;
			 }
		 }
	 }
	 if (prefx == -1 && prefy == -1)
	 {
		 return NULL;
	 }
	 else
	 {
		Move *move= new Move(prefx, prefy);
		board->doMove(move, playerside);
		return move;
	 }
}
